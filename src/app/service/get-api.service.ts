import { Injectable } from '@angular/core';
import axios from 'axios';
@Injectable({
  providedIn: 'root'
})

export class GetApiService {

  constructor() { }

  getJson() {
    return axios.get('https://diplom-dynamic-default-rtdb.europe-west1.firebasedatabase.app/');
  }
}
